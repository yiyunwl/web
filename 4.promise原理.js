

const f = new Promise(function (resolve, reject) {
    setTimeout(function () {
        let data = '请求获取的数据';
        resolve(data);
        let err = '数据读取失败';
        reject(err)
    }, 2000)
})

f.then(function (value) {
    console.log(value)
}, function (reason) {
    console.error(reason)
})
