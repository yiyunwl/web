//1、引用fs模块
const fs = require('fs');

//2、调用方法读取数据
// fs.readFile('./git.txt',function(err,res){
//     if(err) throw err;
//     console.log(res.toString())
// })


//3、promise封装读取数据

const p = new Promise(function(resolve,revloe) {
    fs.readFile('./git.txt',function(err,data){
        if(err) revloe(err);
        resolve(data);

    })
})

p.then(function(res) {
    console.log(res.toString())
},function(err) {
    console.log('读取错误')
})


// fs.readFile('./3.箭头函数.html',function(err,res) {
//     if(err) throw err;
//     console.log(res.toString())
// })